#!/bin/bash

# We need to have an LCG_contrib_${PLATFORM}.txt file in /cvmfs/sft.cern.ch/lcg/releases
PLATFORM=$1
echo PLATFORM is $PLATFORM
# we need to drop micro architecture from the PLATFORM
PLATFORM=$( echo $PLATFORM |  sed "s/+[+a-zA-Z0-9]*-/-/" )
echo After potenial cleaning PLATFORM is $PLATFORM
THE_FILE=/cvmfs/sft.cern.ch/lcg/releases/LCG_contrib_${PLATFORM}.txt
if [ -f ${THE_FILE} ]; then
    echo "LCG_contrib file ${THE_FILE} exists, all good!"
else
    echo "LCG_contrib file ${THE_FILE} does not exist! YOU HAVE TO MAKE IT NOW!"
    exit 1
fi
