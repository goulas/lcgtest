#!/usr/bin/env python
"""
Script to recursively get the dependencies declared in the buildinfo files for a given package.
"""

import os
import sys

from pprint import pprint, pformat

PACKAGE_LOCATIONS = ['/cvmfs/sft.cern.ch/lcg/latest/',
                     '/cvmfs/sft.cern.ch/lcg/releases/',
                     '/cvmfs/sft.cern.ch/lcg/latest/MCGenerators/',
                     '/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/',
                     '/cvmfs/sft.cern.ch/lcg/latest/Grid/',
                     '/cvmfs/sft.cern.ch/lcg/releases/Grid/',
                     ]
NIGHTLIES_LOCATION = '/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/'

BUILD_LOCATIONS = []
if 'BUILD_PATH' in os.environ:
  buildPath = os.environ.get('BUILD_PATH')
  BUILD_LOCATIONS.append(buildPath)
  BUILD_LOCATIONS.append(os.path.join(buildPath, 'MCGenerators'))
  BUILD_LOCATIONS.append(os.path.join(buildPath, 'Grid'))

TRANS_DICT = {}

def readPackageTransDict(dictFile):
  global TRANS_DICT
  packageDict = dictFile
  # read pythonPackage <-> lcgpackage dict
  with open(packageDict) as pacDict:
    for line in pacDict.readlines():
      if line.startswith('#'):
        continue
      pyName, lcgName = line.strip().split(':')
      TRANS_DICT[pyName] = lcgName


def readBuildinfo(buildInfoFile):
    """Read buildinfo files from lcg packages, taken from lcgenv."""
    res = {"HOME": os.path.dirname(buildInfoFile),
           "PLATFORM": os.path.basename(os.path.dirname(buildInfoFile))}
    line = open(buildInfoFile, 'r').readlines()[0].strip()
    for par in line.split(', '):
        key, value = par.split(':', 1)
        res.update({key: value.strip()})
    if ',' in res['VERSION']:
        deps = res['VERSION'].split(',')[1:-1]
        res['VERSION'] = res['VERSION'].split(',')[0]
        res['DEPENDENCIES'] = deps
    if 'DEPENDS' in res:
        deps = res['DEPENDS'].split(',')
        res['DEPENDS'] = [dep for dep in deps if dep]
    return res

def getBuildInfoFile(packageName, version, pHash, platform):
  """Find the buildinfo file for given package, version, hash."""
  for packagePathName in set([packageName,
                              packageName.lower(), # for PyYAML/pyyaml
                              packageName.replace('_', '-'),  # for srm_ifce/srm-ifce
                              TRANS_DICT.get(packageName, packageName)]):

    for loc in PACKAGE_LOCATIONS:
      bInfoFile = os.path.join(loc, packagePathName, f"{version}-{pHash}", platform, f".buildinfo_{packageName}.txt" )
      if os.path.exists(bInfoFile):
        return bInfoFile

    for loc in BUILD_LOCATIONS:
      bInfoFile = os.path.join(loc, packagePathName, f"{version}", platform, f".buildinfo_{packageName}.txt" )
      if os.path.exists(bInfoFile):
        return bInfoFile

    for devs in os.scandir(NIGHTLIES_LOCATION):
      if devs.is_file():
        continue
      for days in os.scandir(devs):
        if days.is_file():
          continue
        for subDir in {'', 'Grid', 'MCGenerators'}:
          bInfoFile = os.path.join(days.path, subDir, packagePathName, f"{version}", platform, f".buildinfo_{packageName}.txt")
          if os.path.exists(bInfoFile) and f"HASH: {pHash}" in open(bInfoFile).read():
            return bInfoFile

  raise FileNotFoundError(f"No buildinfo file found for {packageName} {version} {pHash}")

def findDependencies(buildInfoFile, dependencies):
  """Find the buildinfo of the dependencies and recurse."""
  baseBuildInfo = readBuildinfo(buildInfoFile)
  platform = baseBuildInfo.get('PLATFORM')
  deps = baseBuildInfo.get('DEPENDS', [])
  for package in deps:
    packageName, rest = package.split('-', 1)
    version, pHash = rest.rsplit('-', 1)
    if packageName in dependencies:
      continue
    dependencies.add(packageName)
    # find the buildInfo file of the dependencie
    depBuildInfo = getBuildInfoFile(packageName, version, pHash, platform)
    findDependencies(depBuildInfo, dependencies)

def normalizeSet(setOfPackages):
  """Sort and lowercase the list of packages."""
  normalizedPackages = set()
  for package in setOfPackages:
    package = package.replace('_', '')
    package = package.lower()
    normalizedPackages.add(package)
  return sorted(normalizedPackages)

def getDependencies():
  """Get the dependencies for given package."""

  if len(sys.argv) < 4:
    print("Arguments missing: get_dependencies buildInfo outputFile package_name.dict")
    return 1

  buildInfoFile = sys.argv[1]
  outputFile = sys.argv[2]
  readPackageTransDict(sys.argv[3])
  dependencies = set()

  findDependencies(buildInfoFile, dependencies)

  dependencies = normalizeSet(dependencies)
  with open(outputFile, 'wt') as output:
    for package in dependencies:
      output.write(f"{package}\n")

  return 0

if __name__ == "__main__":
  sys.exit(getDependencies())
