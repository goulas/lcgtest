#!/bin/bash

# run "pip check" for dependency version validation
# FIXME: we ignore lines with 0.0.0 because that means our installation does not set the version correctly.
# Also ignore some packages where we have irreconcilable version requirements

pip check \
          |  grep -v "coffea 0.7.13 has requirement numpy<1.22,>=1.16.0, but you have numpy 1.22.3." \
          |  grep -v "httpstan 4.4.2 has requirement lz4<4.0,>=3.1, but you have lz4 4.0.0." \
          |  grep -v "pyparser 1.0 has requirement parse==1.6.5, but you have parse 1.19.0." \
          |  grep -v "pyrdf 0.2.1 requires enum34, which is not installed." \
          |  grep -v "tensorflow 2.8.0 requires tf-estimator-nightly, which is not installed." \
          | grep -v "tensorflow-cpu 2.12.0 has requirement gast<=0.4.0,>=0.2.1, but you have gast 0.5.2." \
          | grep -v "tensorflow 2.12.0 has requirement gast<=0.4.0,>=0.2.1, but you have gast 0.5.2." \
          | grep -v "tensorboardx 2.5.1 has requirement protobuf<=3.20.1,>=3.8.0, but you have protobuf 3.20.3." \
          | grep -v "kfp 1.8.14 requires google-cloud-storage, which is not installed." \
          | grep -v "google-api-python-client 1.12.11 requires google-auth-httplib2, which is not installed." \
          | grep -v "google-api-python-client 1.12.11 requires httplib2, which is not installed." \
          | grep -v "kfp 1.8.14 has requirement jsonschema<4,>=3.0.1, but you have jsonschema 4.16.0." \
          | grep -v "kfp 1.8.14 has requirement kubernetes<19,>=8.0.0, but you have kubernetes 23.3.0." \
          | grep -v "kfp 1.8.14 has requirement PyYAML<6,>=5.3, but you have pyyaml 6.0." \
          | grep -v "kfp 1.8.14 has requirement google-auth<2,>=1.6.1, but you have google-auth 2.17.3." \
          | grep -v "coffea 0.7.13 has requirement numpy<1.22,>=1.16.0, but you have numpy 1.23.5." \
          | grep -v "qastle 0.16.0 requires lark-parser, which is not installed." \
          | grep -v "servicex 2.6.2 has requirement idna==2.10, but you have idna 3.2." \
          | grep -v "pytimber [.0-9]* requires nxcals, which is not installed." \
          | grep -v "zfit 0.16.0 requires tensorflow, which is not installed." \
          | grep -v "zfit 0.16.0 requires tensorflow-probability, which is not installed." \
          | grep -v "grpcio-status 1.56.2 has requirement protobuf>=4.21.6, but you have protobuf 3.20.3." \
    || echo "No version issues found"
