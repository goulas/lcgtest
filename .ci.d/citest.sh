#!/bin/bash

set -eux

mkdir -p build
cd build

# three letter day of the week
WEEKDAY=$(date +%a)

cmake -D PLATFORM=x86_64-${SETUP}-opt -D BUILD_PATH=/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/${WEEKDAY} ..

ctest -T Test --output-on-failure --no-compress-output  --test-output-size-failed 1024000
