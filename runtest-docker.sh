PATTERN=$1
VIEW=$2

if [[ ! "$PATTERN" ]]; then
    PATTERN=".*"
fi

if [[ ! "$PLATFORM" ]]; then
    export PLATFORM=`$(dirname $0)/../lcgjenkins/getPlatform.py`
fi
SAVED_PLATFORM=$PLATFORM

if [[ ! -e "test_build/Testing/TAG" ]]; then
    mkdir -p test_build/Testing
    if [[ -e /lcgtest/TAG ]]; then
        cp /lcgtest/TAG test_build/Testing
    else
        #TODO Create new TAG based on current time (Default ctest action without APPEND)
        exit 1
    fi
fi

if [[ ! "$VIEW" ]]; then
if [[ "$LCG_VERSION" == *"dev"* ]]; then
    VIEW="/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/latest/$PLATFORM"
else
    VIEW="/cvmfs/sft.cern.ch/lcg/views/$LCG_VERSION/$PLATFORM"
fi  
fi

source $VIEW/setup.sh
if [[ $SAVED_PLATFORM == *slc6* ]]; then
    export KERAS_BACKEND=theano
fi

if [[ ! -e "test_build" ]]; then
    mkdir test_build
fi
cd test_build
# Cut ubuntu platform name
PLATFORM=$(echo $SAVED_PLATFORM | sed 's/ubuntu\([0-9]\{2\}\)[0-9]\{2\}/ubuntu\1/')
ctest -VV -DPLATFORM=$PLATFORM -DCTEST_LABELS="$PATTERN" -S ../lcgtest/lcgtest.cmake
